package in.nareshit.raghu.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.nareshit.raghu.entity.Customer;
import in.nareshit.raghu.service.ICustomerService;
import in.nareshit.raghu.service.exception.CustomerNotFoundException;

@RequestMapping("/customer")
@RestController
public class CustomerRestController {
	
	@Autowired
	private ICustomerService service;
	
	@PostMapping("/create")
	public ResponseEntity<String> createCustomer(@RequestBody Customer customer){
		ResponseEntity<String>response =null;
		Long id = service.saveCustomer(customer);
		response = ResponseEntity.ok("Customer '"+id+"' created");
		return response;
	}
	
	@GetMapping("/all")
	public ResponseEntity<List<Customer>>getAllCustomers(){
		ResponseEntity<List<Customer>>response = null;
		List<Customer>list = service.getAllCustomers();
		response = ResponseEntity.ok(list);
		return response;
	}
	
	@GetMapping("/find/{mail}")
   public ResponseEntity<Customer>getOneCustomerByEmail(@PathVariable String mail){
		ResponseEntity<Customer>response = null;
		try {
	          Customer cust = service.getOneCustomerByEmail(mail);
	          response = new ResponseEntity<Customer>(cust, HttpStatus.OK);
		}
		catch(CustomerNotFoundException cnfe) {
			cnfe.printStackTrace();
			throw cnfe;
		}
		return response;
   }
	
	
//	@GetMapping("/find/{pancard}")
//	   public ResponseEntity<Customer>getOneCustomerByPancard(@PathVariable String pancard){
//			ResponseEntity<Customer>response = null;
//			try {
//		          Customer cust = service.getOneCustomerByPanCard(pancard);
//		          response = new ResponseEntity<Customer>(cust, HttpStatus.OK);
//			}
//			catch(CustomerNotFoundException cnfe) {
//				cnfe.printStackTrace();
//				throw cnfe;
//			}
//			return response;
//	   }
//
//
//	
//	@GetMapping("/find/{aadhar}")
//	   public ResponseEntity<Customer>getOneCustomerByAadhar(@PathVariable String aadhar){
//			ResponseEntity<Customer>response = null;
//			try {
//		          Customer cust = service.getOneCustomerByAadhar(aadhar);
//		          response = new ResponseEntity<Customer>(cust, HttpStatus.OK);
//			}
//			catch(CustomerNotFoundException cnfe) {
//				cnfe.printStackTrace();
//				throw cnfe;
//			}
//			return response;
//	   }
//
//
//	
//	@GetMapping("/find/{mobile}")
//	   public ResponseEntity<Customer>getOneCustomerByMobile(@PathVariable String mobile){
//			ResponseEntity<Customer>response = null;
//			try {
//		          Customer cust = service.getOneCustomerByMobile(mobile);
//		          response = new ResponseEntity<Customer>(cust, HttpStatus.OK);
//			}
//			catch(CustomerNotFoundException cnfe) {
//				cnfe.printStackTrace();
//				throw cnfe;
//			}
//			return response;
//	   }
//
//

}
